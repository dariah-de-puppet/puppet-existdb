# eXistdb Puppet module

Customized and enhanced for use in RDD.

## Attention

This module now defaults to loading the distribution packages from a [GWDG Gitlab Repo](https://gitlab.gwdg.de/sepia/existdb/-/packages).

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with existdb](#setup)
    * [Beginning with existdb](#beginning-with-existdb)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

This module installs the eXist database software and starts it as a service.

## Setup

To use this module, add these declarations to your Puppetfile:

```txt
mod 'puppetlabs-stdlib','4.25.1'
mod 'puppetlabs-java', '2.1.0'
mod 'puppet-archive', '3.0.0'
mod 'puppet-nginx','0.15.0'
mod 'jonhallettuob-existdb', '0.3.3'
```

To install eXistdb and start it as a service with default parameters:

```puppet
class { 'java':
  package => 'java-1.8.0-openjdk-devel',
}
class { 'existdb': }
```

Or equivalently in Hiera:

```yaml
---
classes:
  - java
  - existdb

java::package: 'java-1.8.0-openjdk-devel'
```

## Usage

Set up eXistdb and its data in specific directories:

```puppet
class existdb {
  exist_home => '/usr/local/exist',
  exist_data => '/var/lib/exist',
}
```

## Service Configuration

When another service name (`$exist_service`) than the default ('eXist-db') is provided, the module looks after a configuration file `puppet:///modules/profiles/etc/systemd/system/${exist_service}.service`

## Reference

```puppet
class existdb
(
  $exist_home                  = '/usr/local/existdb',
  $exist_data                  = '/var/lib/existdb',
  $exist_cache_size            = '128M',
  $exist_collection_cache_size = '24M',
  $exist_revision              = 'eXist-5.2.0',
  $exist_version               = regsubst($exist_revision, '^eXist-', ''),
  $java_home                   = '/usr/lib/jvm/jre',
  $exist_user                  = 'existdb',
  $exist_group                 = 'existdb',
  $exist_service               = 'eXist-db',
  $running                     = true,
)
{
 ...
}
```

## Limitations

The module was developed and tested on Ubuntu 18.04/20.04 with Puppet 5.5/6.
